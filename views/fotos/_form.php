<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Articulo;

/* @var $this yii\web\View */
/* @var $model app\models\Fotos */
/* @var $form yii\widgets\ActiveForm */

$articulos=Articulo::find()->all();

$listado=ArrayHelper::map($articulos,'id','titulo');    // Es mejor hacerlo en el modelo o controller

//var_dump($listado);

//echo $form->field($model, 'titulo')->dropDownList(
//        $listado,
//        ['prompt'=>'Select...']
//        );

?>

<div class="fotos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'articulo')->dropDownList($listado) ?>     <!-- Ésto es lo comentado de arriba: echo $form->field($model, 'titulo')->dropDownList -->

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alt')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
