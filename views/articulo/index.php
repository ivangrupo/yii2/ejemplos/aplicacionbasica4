<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Artículos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articulo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir Artículo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'titulo',
            'texto:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {fotos} {delete}',
                'buttons'=>[
<<<<<<< HEAD
                    'delegacion'=>function($url,$model,$key){
=======
                    'fotos'=>function($url,$model,$key){
>>>>>>> bb40fc67a67275b49dc89ded30faa53065516d20
                        //return Html::a('<span class="glyphicon glyphicon-camera"></span>',$url);
                        return Html::a('<span class="glyphicon glyphicon-camera"></span>',[
                            'foto/listar',"id"=>$model->id,
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
