<?php

    use yii\helpers\Html;
    
?>


<h1>
    <?= $this->title = 'Foto ' . $datos->articulo; ?>
</h1>

<p>
    <?= $datos->alt ; ?>
</p>

<div>
    <?= Html::img('@web/imgs/' . $datos->nombre, ['alt' => 'Foto']); ?>
</div>

