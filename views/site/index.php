<?php

use app\models\Fotos;

/* @var $this yii\web\View */

$this->title = 'Aplicación Básica 4 Yii 2';

//var_dump($articulos);

?>
<div class="site-index">
    <ul>
        <?php
        
        foreach ($articulos as $articulo) {
            echo $this->render('index/_articulo', [
                "datos" => $articulo,
            ]);
        
        
        // VER LAS FOTOS DEL ARTÍCULO ACTUAL
        
        $fotosArticulo = $articulo->fotos;  // Array del modelo fotos
        foreach ($fotosArticulo as $foto) {
        
            echo $this->render('index/_fotos', [
                "datos" => $foto,
            ]);
            
//            Fotos::find()->all; // Todas las fotos
            
//            $a = $articulo->fotos; // Es lo mismo que lo de abajo y es MEJOR
//            var_dump($a);
//            
//            $a = Fotos::find()
//                    ->where(["articulo"=>$articulo->id])
//                    ->all();
//            var_dump($a);
        }
        }
        ?>
    </ul>
</div>
