<?php

    use yii\helpers\Html;
    use yii\bootstrap\Nav;
    
    $this->params['breadcrumbs'][] = $this->title = 'Back';

?>

    <ul class="nav nav-pills nav-stacked">
        <li role="presentation">
            <?= Html::a('Artículos', ['articulo/index'], ["class" => "btn bg-info"]) ?>
        </li>
      <li role="presentation">
          <?= Html::a('Fotos', ['fotos/index'], ["class" => "btn bg-info"]) ?>
      </li>
    </ul>


<br>
<div class="index">
<!--< ?=
        Nav::widget([
        'options' => ['class' => 'nav nav-pills nav-stacked'],		// ES LO MISMO QUE ARRIBA PERO UTILIZANDO Nav::widget
        'items' => [
            ['label' => 'Artículos', 'url' => ['articulo/index']],
            ['label' => 'Fotos', 'url' => ['fotos/index']],
        ],
    ]);
        
? >-->

</div>

