﻿--
-- Definition for database aplicacionbasica4
--
DROP DATABASE IF EXISTS aplicacionbasica4;
CREATE DATABASE IF NOT EXISTS aplicacionbasica4
	CHARACTER SET utf8
	COLLATE utf8_spanish_ci;

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Set default database
--
USE aplicacionbasica4;

--
-- Definition for table articulo
--
CREATE TABLE IF NOT EXISTS articulo (
  id INT(11) NOT NULL AUTO_INCREMENT,
  titulo VARCHAR(255) NOT NULL,
  texto TEXT DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_spanish_ci;


--
-- Definition for table foto
--
CREATE TABLE IF NOT EXISTS foto (
  id INT(11) NOT NULL AUTO_INCREMENT,
  articulo INT(11),
  nombre VARCHAR(255) DEFAULT NULL,
  alt VARCHAR(255),
  PRIMARY KEY (id),
  UNIQUE KEY (articulo,nombre),
  FOREIGN KEY(articulo) REFERENCES articulo(id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_spanish_ci; 

-- 
-- Dumping data for table articulo
--
INSERT INTO articulo VALUES
(1, 'Empezando', 'Comienza lo bueno'),
(2, 'Programando', 'En clase probando Yii 2'),
(3, 'Vacaciones', 'Trabajando en clase'),
(4, 'Lorem', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus dignissim egestas mi eu tristique. In et enim id sem euismod pharetra. Etiam lacinia efficitur dignissim. Phasellus ut tristique diam. Praesent accumsan sapien erat, sed interdum sapien sodales non. Donec id tellus enim. Fusce nibh nunc, rutrum eget leo ac, imperdiet aliquam massa. Proin ullamcorper diam tortor, in condimentum nulla scelerisque id. Aenean at est mollis, pretium neque vel, porta arcu. Ut dapibus, ante a egestas rutrum, est dui dapibus lectus, sed laoreet metus massa ac libero. Proin pellentesque mi in pretium dictum. Nulla facilisi.');


-- 
-- Dumping data for table articulo
--
INSERT INTO foto VALUES
(1, 1, '1.jpg', 'Ejemplo de foto en el artículo'),
(2, 2, '2.jpg', 'Ejemplo 2 de artículo'),
(3, 3, '3.jpg', 'Ejemplo 3 en clase'),
(4, 4, '4.jpg', 'Ejemplo 4 Lorem');


-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;