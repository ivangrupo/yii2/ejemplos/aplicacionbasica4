<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Articulo;
use app\models\Fotos;



class SiteController extends Controller {
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $datos = Articulo::find()->all();   // Array de objetos
        
        return $this->render('index', [
            "articulos" => $datos,  //  $datos es toda la información de la la tabla de la base de datos desde el controller y "articulos" son los datos que se pasan a las views
        ]);
    }
    
    public function actionBack() {
        
        return $this->render('back');
        
//        $datos = Articulo::find()->all();   // Array de objetos
        
//        return $this->render('back', [
//            "articulos" => $datos,  //  $datos es toda la información de la la tabla de la base de datos desde el controller y "articulos" son los datos que se pasan a las views
//        ]);
    }

}
